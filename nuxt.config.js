const webpack = require('webpack');

module.exports = {
  /*
  ** Headers of the page
  */
  head: {
    title: 'agi-orderer',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: 'Nuxt.js project' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      {
        rel: 'stylesheet',
        href: 'https://fonts.googleapis.com/css?family=Sacramento|Indie+Flower'
      },
      {
        rel: 'stylesheet',
        href: 'https://fonts.googleapis.com/earlyaccess/roundedmplus1c.css'
      }
    ]
  },
  css: ['~/assets/base.css'],
  /*
  ** Customize the progress bar color
  */
  loading: { color: '#3B8070' },
  /*
  ** Build configuration
  */
  build: {
    /*
    ** Run ESLint on save
    */
    extend(config, { isDev, isClient }) {
      if (isDev && isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/
        });
      }
      config.plugins.push(
        new webpack.EnvironmentPlugin([
          'FB_API_KEY',
          'FB_AUTH_DOMAIN',
          'FB_DATABASE_URL',
          'FB_PROJECT_ID',
          'FB_STORAGE_BUCKET',
          'FB_MESSAGING_SENDER_ID'
        ])
      );
    }
  },
  modules: ['@nuxtjs/font-awesome']
};
