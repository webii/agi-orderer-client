import Vue from 'vue';
import Vuex from 'vuex';
import firebase from '~/plugins/firebase';
require('firebase/firestore');

const db = firebase.firestore();

Vue.use(Vuex);

const createStore = () => {
  return new Vuex.Store({
    state: {
      items: [],
      order: {}
    },
    getters: {
      items: state =>
        state.items.map(item => {
          return {
            ...item,
            amount:
              typeof state.order[item.id] !== 'undefined'
                ? state.order[item.id].amount
                : 0
          };
        }),
      order: state => ({ ...state.order })
    },
    mutations: {
      initializeItems: (state, { items }) => {
        state.items = [...items.map(item => ({ ...item }))];
      },
      increaseOrderAmount: (state, { id, amount }) => {
        if (typeof state.order[id] !== 'undefined') {
          state.order = {
            ...state.order,
            [id]: {
              ...state.order[id],
              amount: state.order[id].amount + amount
            }
          };
        } else {
          state.order = {
            ...state.order,
            [id]: {
              detail: state.items.find(item => item.id === id),
              amount
            }
          };
        }
        console.log(state);
      },
      cleanOrder: state => {
        state.order = {};
      }
    },
    actions: {
      INIT_ITEMS: async ({ commit }) => {
        // firestoreからメニューを取得
        const querySnapshot = await db.collection('items').get();
        const items = [];
        querySnapshot.forEach(doc => items.push(doc.data()));
        // 要素のidで昇順に並べる
        items.sort((a, b) => a.id - b.id);
        commit('initializeItems', {
          items
        });
      },
      INCREASE_ORDER_AMOUNT: ({ commit }, { id, amount }) => {
        commit('increaseOrderAmount', { id, amount });
      },
      SUBMIT_ORDER: async ({ commit }) => {
        // submit order to firestore.
        await db
          .collection('orders')
          .doc(new Date().getTime().toString())
          .set({ tableNumber: 7 });
        // clean submitted order.
        commit('cleanOrder');
      }
    }
  });
};

export default createStore;
